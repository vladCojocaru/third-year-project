function getBaseLog(x, y) {
  return Math.log(y) / Math.log(x);
}
// This function will take all data gathered in the fsm about states, transitions outputs etc and create
// a compilable verilog module
export function compileVerilog(jsonFSM, vm, outputsForStates) {
  const reset = "";
  const size = vm.availableStates().length;
  var textVerilog = "";
  textVerilog += "module fsm (\n";
  textVerilog += "clock";
  vm.availableInputs().forEach((input) => {
    textVerilog += `,\n${input}`;
  });
  textVerilog += "\n);\n";
  textVerilog += "input clock";
  if (vm.checkedReset()) {
    textVerilog += ",reset";
  }
  vm.availableInputs().forEach((input) => {
    textVerilog += `, ${input}`;
  });
  textVerilog += ";\n";

  textVerilog += "output ";
  vm.availableOutputs().forEach((input, index) => {
    textVerilog += `${input}`;
    if (index != vm.availableOutputs().length - 1) textVerilog += `,`;
  });
  textVerilog += ";\n";

  textVerilog += "reg ";
  vm.availableOutputs().forEach((input, index) => {
    textVerilog += `${input}`;
    if (index != vm.availableOutputs().length - 1) textVerilog += `,`;
  });
  textVerilog += ";\n";

  // Wire declaration
  textVerilog += "wire clock";
  if (vm.checkedReset()) {
    textVerilog += ",reset";
  }
  vm.availableInputs().forEach((input) => {
    textVerilog += `, ${input}`;
  });
  textVerilog += ";\n";
  // Parameter declaration
  textVerilog += `parameter SIZE = ${size};\n`;
  textVerilog += `parameter `;
  const varAsBits = Math.ceil(getBaseLog(2, size));
  vm.availableStates().forEach((state, index) => {
    textVerilog += `${state.toUpperCase()} = ${varAsBits}'d${index}`;
    if (index != size - 1) textVerilog += `,`;
  });

  textVerilog += ` ;\n`;

  textVerilog += `reg [${size - 1}:0]       state;\n`;
  textVerilog += `reg [${size - 1}:0]       next_state;\n`;
  //--State Transition
  textVerilog += "always @ (state)\n";
  textVerilog += `begin\n  case(state)\n`;
  // The function will account for correct spacing and will create perfectly formatted code
  // All states will be taken to uppercase
  vm.availableStates().forEach((state) => {
    if (
      jsonFSM.transitions.find((transition) => {
        if (transition.from == state) return true;
      })
    ) {
      textVerilog += `    ${state.toUpperCase()}: begin \n`;
      const spaces = state.length;
      var fromTransitions = [];
      jsonFSM.transitions.forEach((transition) => {
        if (transition.from == state) {
          fromTransitions.push(transition);
        }
      });

      fromTransitions.forEach((transition, index) => {
        textVerilog += `        ` + Array(spaces).fill(" ").join("");
        const transitionStringArr = transition.label
          .split(`\n`)
          .filter((element) => element != "");
        textVerilog += `if (`;
        transitionStringArr.forEach((transitionPart, index) => {
          if (
            transitionStringArr.length > 1 &&
            index == transitionStringArr.length - 1
          )
            textVerilog += `(${transitionPart})`;
          else if (transitionStringArr.length > 1) {
            let firstPart = transitionPart.substr(0, transitionPart.length - 3);
            let lastPart = transitionPart.substr(transitionPart.length - 3);
            let newString = firstPart + ")" + lastPart;
            textVerilog += `(${newString}`;
          } else textVerilog += `${transitionPart}`;
          if (index == transitionStringArr.length - 1) {
            textVerilog += `) begin\n`;
          } else {
            textVerilog += `\n` + `        ` + Array(spaces).fill(" ").join("");
          }
        });
        textVerilog +=
          `          ` +
          Array(spaces).fill(" ").join("") +
          `next_state = ${transition.to.toUpperCase()};\n`;
        textVerilog += `        ` + Array(spaces).fill(" ").join("") + `end`;
        if (index != fromTransitions.length - 1) {
          textVerilog += ` else\n`;
        } else {
          textVerilog += `\n`;
        }
      });
      textVerilog += `      ` + Array(spaces).fill(" ").join("") + `end\n`;
    }
  });
  textVerilog += `  endcase\nend\n`;

  // Seq Logic
  textVerilog += "always @ (posedge clock)\nbegin\n";

  if (vm.checkedReset()) {
    textVerilog += `  if (reset == 1'b1) begin\n`;
    textVerilog += `    state <= ${vm.availableStates()[0].toUpperCase()};\n`;
    textVerilog += "  end else begin\n";
    textVerilog += "    state <= next_state;\n  end\nend\n";
  } else {
    textVerilog += "    state <= next_state;\nend\n";
  }

  // Output logic
  textVerilog += "always @ (posedge clock)\n";
  textVerilog += "begin\n";
  textVerilog += "  case(state)\n";
  vm.availableStates().forEach((state) => {
    if (
      outputsForStates.find((output) => {
        if (output.state == state) return true;
      })
    ) {
      const spaces = state.length;
      textVerilog +=
        `    ${state.toUpperCase()}: begin \n` +
        `        ` +
        Array(spaces).fill(" ").join("");

      var outputsStringArr = outputsForStates.find((output) => {
        if (output.state == state) return output;
      });
      outputsStringArr = outputsStringArr.outputs;

      outputsStringArr = outputsStringArr
        .split(`\n`)
        .filter((element) => element != "");
      outputsStringArr.forEach((outputPart, index) => {
        textVerilog += `${outputPart};`;
        if (index == outputsStringArr.length - 1) {
          textVerilog +=
            `\n      ` + Array(spaces).fill(" ").join("") + `end\n`;
        } else {
          textVerilog += `\n` + `        ` + Array(spaces).fill(" ").join("");
        }
      });
    }
  });

  textVerilog += "  endcase\nend\nendmodule";

  return textVerilog;
}
