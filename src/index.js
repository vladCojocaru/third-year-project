// Written by Vlad Andrei Cojocaru University Of Manchester 2021

// Import client framework
import ko from "knockout";
// Import FSM renderer to show the graphical representation of the FSM
import { render } from "state-machine-cat";
import { compileVerilog } from "./compile";
// Used to save transition and output logic in a string format
var dataToSaveAsString;
// Contains each transition with its boolean table that depends on the inputs
var transitionsTable = [];
// Contains all FSM data that needs to be downloaded for it to be then uploaded
var downloadableFSM = {};
var outputsForStates = [];
// Represents each line of the transition table, each of them being an verilog expression
var TransitionTableLine = function () {
  /*
    inputs represents the selected input in the first collumn of the table
    selectedSign represents the sign (==,!=,etc) selected in the second row of the transition table
    selectedFirstValueBool or Input represent the option given to use a boolean value or a input as a operand for the expression
    selectedFirstLogic represents the logical AND or OR chosen by the user with any parenthesis chosen
  */
  var self = this;
  self.inputs = ko.observable();
  self.selectedSign = ko.observable();
  self.selectedFirstValueBool = ko.observable();
  self.selectedFirstValueInput = ko.observable();
  self.selectedFirstLogic = ko.observable();
  self.selectedSecondValueBool = ko.observable();
  self.selectedSecondValueInput = ko.observable();
  self.selectedSecondLogic = ko.observable();
  self.selectedThirdValueBool = ko.observable();
  self.selectedThirdValueInput = ko.observable();
  self.selectedFinalLogic = ko.observable();
  // The closing brackets will automatically add enough parenthesis to the expression for it be compilable
  self.closingBrackets = ko.computed(function () {
    var bracketNumber = 0;
    if (self.selectedSign() == "==(" || self.selectedSign() == "!=(")
      bracketNumber++;
    if (
      self.selectedFirstLogic() == "&&(" ||
      self.selectedFirstLogic() == "||("
    )
      bracketNumber++;
    if (
      self.selectedSecondLogic() == ")&&" ||
      self.selectedSecondLogic() == ")||"
    )
      bracketNumber--;
    switch (bracketNumber) {
      case 0:
        return "";
      case 1:
        return ")";
      case 2:
        return "))";
      default:
        return "";
    }
  });

  // The final logic is used when the user wants to add more expressions to a transition
  // These expressions are linked through logic that is enabled only when other logic is not
  self.enableFinalLogic = ko.computed(function () {
    if (
      (self.selectedFirstValueBool() != "notSelected" ||
        self.selectedFirstValueInput() != undefined) &&
      self.selectedFirstLogic() == "notSelected"
    )
      return true;
    else if (
      (self.selectedSecondValueBool() != "notSelected" ||
        self.selectedSecondValueInput() != undefined) &&
      self.selectedSecondLogic() == "notSelected"
    )
      return true;
    else if (
      self.selectedThirdValueBool() != "notSelected" ||
      self.selectedThirdValueInput() != undefined
    )
      return true;
    else {
      self.selectedFinalLogic("notSelected");
      return false;
    }
  });

  /* 
    The add transition table is composed of lines and each line has multiple operands.
    To aid in the use of the table, each operand is invisible until the user selects the one before it.
    The functions below implement that behaviour and if the user deselects any operand in the line all
      other operands following on that line will dissapear and deselect in a domino effect.
    The framework works in such a way that when one operand is deselected it's "listener" will deselect the next
      one and that one will deselect the next one and so on.
  */
  self.inputs.subscribe(function () {
    if (self.inputs() == undefined) {
      self.selectedSign("notSelected");
    }
  });
  self.selectedSign.subscribe(function () {
    if (self.selectedSign() == "notSelected") {
      self.selectedFirstValueBool("notSelected");
      self.selectedFirstValueInput(undefined);
    }
  });
  self.selectedFirstValueBool.subscribe(function () {
    if (self.selectedFirstValueBool() == "notSelected") {
      self.selectedFirstLogic("notSelected");
    }
  });
  self.selectedFirstValueInput.subscribe(function () {
    if (self.selectedFirstValueInput() == undefined) {
      self.selectedFirstLogic("notSelected");
    }
  });
  self.selectedFirstLogic.subscribe(function () {
    if (self.selectedFirstLogic() == "notSelected") {
      self.selectedSecondValueBool("notSelected");
      self.selectedSecondValueInput(undefined);
    }
  });
  self.selectedSecondValueBool.subscribe(function () {
    if (self.selectedSecondValueBool() == "notSelected") {
      self.selectedSecondLogic("notSelected");
    }
  });
  self.selectedSecondValueInput.subscribe(function () {
    if (self.selectedSecondValueInput() == undefined) {
      self.selectedSecondLogic("notSelected");
    }
  });
  self.selectedSecondLogic.subscribe(function () {
    if (self.selectedSecondLogic() == "notSelected") {
      self.selectedThirdValueBool("notSelected");
      self.selectedThirdValueInput(undefined);
    }
  });
  self.selectedThirdValueBool.subscribe(function () {
    if (self.selectedThirdValueBool() == "notSelected") {
      self.selectedFinalLogic("notSelected");
    }
  });
  self.selectedThirdValueInput.subscribe(function () {
    if (self.selectedThirdValueInput() == undefined) {
      self.selectedFinalLogic("notSelected");
    }
  });
};

// Holds all of the lines in a transition table and enables adding more lines, deleting,
// saving,etc
var TransitionTable = function () {
  var self = this;
  // Line array
  self.lines = ko.observableArray();
  // Available inputs that can be selected in the table by the user
  self.availableInputs = ko.pureComputed(function () {
    return vm.availableInputs();
  });

  // The state from which and the state to which the transition will added
  self.currentFrom = ko.pureComputed(function () {
    return vm.currentFrom();
  });
  self.currentTo = ko.pureComputed(function () {
    return vm.currentTo();
  });
  // Adding and removing lines
  self.addLine = function () {
    self.lines.push(new TransitionTableLine());
  };
  self.removeLine = function (line) {
    self.lines.remove(line);
  };
  self.removeLines = function () {
    self.lines.removeAll();
  };

  // This verifies wether the save button is enabled.
  // A line must end in either the final logic if there are more lines after it or in a boolean/input variable
  // This function makes sure that this will happen
  self.verifySubmit = ko.computed(function () {
    var found = false;
    self.lines().forEach((line, index) => {
      if (
        line.enableFinalLogic() == false ||
        (line.selectedFinalLogic() == "notSelected" &&
          index != self.lines().length - 1)
      ) {
        found = true;
      }
    });
    // The last line needs to have its final logic not selected
    if (
      self.lines().length != 0 &&
      self.lines()[self.lines().length - 1].selectedFinalLogic() !=
        "notSelected"
    )
      found = true;
    if (found == false && self.lines().length != 0) return true;
    else return false;
  });

  // If the last line of the table has its final logic selected then adding more lines is enabled
  self.verifyAdd = ko.computed(function () {
    console.log(self.lines());
    if (self.lines().length != 0)
      if (
        self.lines()[self.lines().length - 1].selectedFinalLogic() ==
        "notSelected"
      ) {
        return false;
      } else {
        return true;
      }
    else return true;
  });

  /*
    Validation means that when ading a transition a check must be made so that no matter
    the boolean combination of inputs there is no conflict with other transition from the same state
    Conflict - for a boolean combination of inputs 2 transition have 1 as result
    This function parses each line, constructs an array with the result of each line and then calculates
    that array with the logic operators between the lines.
  */
  self.validateTransition = function (dataToSave) {
    var jsonInputs = [];
    self.availableInputs().forEach((input) => {
      jsonInputs.push({ name: input, value: 0 });
    });
    const maxValue = Math.pow(2, jsonInputs.length) - 1;
    var currentValue;
    var finalResultArr = [];
    // We iterate over every combination of inputs
    for (currentValue = 0; currentValue <= maxValue; currentValue++) {
      // From the index above we create it's boolean representation in an array
      // They will serve as the values for the inputs
      const valueAsBinaryArr = ("" + currentValue.toString(2))
        .padStart(jsonInputs.length, "0")
        .split("");

      var i;
      for (i = 0; i < jsonInputs.length; i++) {
        jsonInputs[i].value = parseInt(valueAsBinaryArr[i]);
      }

      var selectedInput;
      var firstValue;
      var secondValue;
      var thirdValue;
      var intermediateArr = [];
      // For each of the lines in the transition
      dataToSave.forEach((currentLine) => {
        // Depending on the line size we have to parse 4 elements.
        // The first will always be an input while the next three can either be
        // inputs or boolean values
        selectedInput = parseInt(
          jsonInputs.find((input) => input.name === currentLine.inputs).value
        );
        if (currentLine.selectedFirstValueBool != "notSelected") {
          firstValue = parseInt(currentLine.selectedFirstValueBool);
        } else if (currentLine.selectedFirstValueInput != undefined) {
          firstValue = parseInt(
            jsonInputs.find(
              (input) => input.name === currentLine.selectedFirstValueInput
            ).value
          );
        }

        if (currentLine.selectedSecondValueBool != "notSelected")
          secondValue = parseInt(currentLine.selectedSecondValueBool);
        else if (currentLine.selectedSecondValueInput != undefined) {
          secondValue = parseInt(
            jsonInputs.find(
              (input) => input.name === currentLine.selectedSecondValueInput
            ).value
          );
        }

        if (currentLine.selectedThirdValueBool != "notSelected")
          thirdValue = parseInt(currentLine.selectedThirdValueBool);
        else if (currentLine.selectedThirdValueInput != undefined) {
          thirdValue = parseInt(
            jsonInputs.find(
              (input) => input.name === currentLine.selectedThirdValueInput
            ).value
          );
        }
        // at the end we now have each operator from the line as a boolean value
        var intermediateResult;
        // Next we have to evaluate the expressions depending on paranthesis line length and && precedence
        // First verifying if the line has three operators then 2 then one and for each case the combination of logic and parenthesis
        if (currentLine.selectedSecondLogic != "notSelected") {
          if (
            currentLine.selectedFirstLogic == "&&" ||
            (currentLine.selectedSign != "==" &&
              currentLine.selectedSign != "!=" &&
              currentLine.selectedSecondLogic != "&&" &&
              currentLine.selectedSecondLogic != "||" &&
              currentLine.selectedFirstLogic != "&&(" &&
              currentLine.selectedFirstLogic != "||(")
          ) {
            if (
              currentLine.selectedFirstLogic.indexOf("&") != -1 &&
              currentLine.selectedSecondLogic.indexOf("&") != -1
            )
              intermediateResult = firstValue && secondValue && thirdValue;
            else if (
              currentLine.selectedFirstLogic.indexOf("&") != -1 &&
              currentLine.selectedSecondLogic.indexOf("|") != -1
            )
              intermediateResult = (firstValue && secondValue) || thirdValue;
            else if (
              currentLine.selectedFirstLogic.indexOf("|") != -1 &&
              currentLine.selectedSecondLogic.indexOf("&") != -1
            )
              intermediateResult = (firstValue || secondValue) && thirdValue;
            else intermediateResult = firstValue || secondValue || thirdValue;
          } else {
            if (
              currentLine.selectedFirstLogic.indexOf("&") != -1 &&
              currentLine.selectedSecondLogic.indexOf("&") != -1
            )
              intermediateResult = firstValue && secondValue && thirdValue;
            else if (
              currentLine.selectedFirstLogic.indexOf("&") != -1 &&
              currentLine.selectedSecondLogic.indexOf("|") != -1
            )
              intermediateResult = firstValue && (secondValue || thirdValue);
            else if (
              currentLine.selectedFirstLogic.indexOf("|") != -1 &&
              currentLine.selectedSecondLogic.indexOf("&") != -1
            )
              intermediateResult = firstValue || (secondValue && thirdValue);
            else intermediateResult = firstValue || secondValue || thirdValue;
          }
        } else if (currentLine.selectedFirstLogic != "notSelected") {
          if (currentLine.selectedFirstLogic.indexOf("&") != -1)
            intermediateResult = firstValue && secondValue;
          else intermediateResult = firstValue || secondValue;
        } else intermediateResult = firstValue;

        if (currentLine.selectedSign.indexOf("!") != -1)
          intermediateResult = intermediateResult != selectedInput ? 1 : 0;
        else intermediateResult = intermediateResult == selectedInput ? 1 : 0;
        intermediateArr.push(intermediateResult);
      });
      var intermediateSignArr = [];
      dataToSave.forEach((line) => {
        if (line.selectedFinalLogic != "notSelected")
          intermediateSignArr.push(line.selectedFinalLogic);
      });
      // After the lines  are evaluated we then evaluate the logic between the lines
      while (intermediateSignArr.indexOf("&&") != -1) {
        var signIndex = intermediateSignArr.indexOf("&&");
        intermediateArr[signIndex] =
          intermediateArr[signIndex] && intermediateArr[signIndex + 1];
        intermediateArr.splice(signIndex + 1, 1);
        intermediateSignArr.splice(signIndex, 1);
      }

      while (intermediateSignArr.indexOf("||") != -1) {
        intermediateArr[0] = intermediateArr[0] || intermediateArr[1];
        intermediateArr.splice(1, 1);
        intermediateSignArr.splice(0, 1);
      }
      finalResultArr.push(intermediateArr[0]);
    }
    console.log(finalResultArr);
    console.log(transitionsTable);
    var transitionTableCopy = transitionsTable;
    // If that state has no transition we add it
    // We have to add its table of boolean values for future transitions to be compared to
    if (!transitionsTable.find((table) => table.from == self.currentFrom())) {
      transitionsTable.push({
        from: self.currentFrom(),
        to: self.currentTo(),
        validationTable: finalResultArr,
      });
      console.log(transitionsTable);
      console.log("success");
      return true;
    } else {
      // If the user is trying to replace a transition we delete the existing transition so that no conflicts with the previous transition
      // will happen
      console.log(self.currentFrom());
      console.log(self.currentTo());
      if (
        transitionsTable.find(
          (table) =>
            table.to === self.currentTo() && table.from === self.currentFrom()
        )
      ) {
        var indexToDelete = transitionsTable.findIndex((table) => {
          return (
            table.to === self.currentTo() && table.from === self.currentFrom()
          );
        });
        transitionsTable.splice(indexToDelete, 1);
      }
      var failed = false;
      var failedTo;
      // If there are any transition for that state compare their tables with the one now to see if there are any conflicts
      transitionsTable.forEach((table) => {
        if (table.from == self.currentFrom()) {
          table.validationTable.forEach((inputVariantResult, index) => {
            if (inputVariantResult && finalResultArr[index] && 1) {
              console.log("fail");
              failed = true;
              failedTo = table.to;
              return;
            }
          });
        }
        if (failed == true) return;
      });
      if (failed == true) {
        transitionsTable = transitionTableCopy;
        return `conflict with transition to ${failedTo}`;
      } else {
        transitionsTable.push({
          from: self.currentFrom(),
          to: self.currentTo(),
          validationTable: finalResultArr,
        });
        return true;
      }
    }
  };
  // Used when the save button is used. it will first use the validation and then convert
  // all elements of the lines into a string that is then added in the FSM
  self.save = function () {
    dataToSaveAsString = "";
    var dataToSave = $.map(self.lines(), function (line) {
      return {
        inputs: line.inputs(),
        selectedSign: line.selectedSign(),
        selectedFirstValueBool: line.selectedFirstValueBool(),
        selectedFirstValueInput: line.selectedFirstValueInput(),
        selectedFirstLogic: line.selectedFirstLogic(),
        selectedSecondValueBool: line.selectedSecondValueBool(),
        selectedSecondValueInput: line.selectedSecondValueInput(),
        selectedSecondLogic: line.selectedSecondLogic(),
        selectedThirdValueBool: line.selectedThirdValueBool(),
        selectedThirdValueInput: line.selectedThirdValueInput(),
        selectedFinalLogic: line.selectedFinalLogic(),
        closingBrackets: line.closingBrackets(),
      };
    });
    // Validate the transition
    var result = self.validateTransition(dataToSave);
    // If the transition is valid convert it into a string
    if (result == true) {
      dataToSave.forEach(function (line, index) {
        if (
          line.selectedSecondValueBool != "notSelected" ||
          line.selectedSecondValueInput != undefined
        )
          dataToSaveAsString += `${line.inputs} ${line.selectedSign} (`;
        else dataToSaveAsString += `${line.inputs} ${line.selectedSign} `;

        if (line.selectedFirstValueBool != "notSelected")
          dataToSaveAsString += `1'b${line.selectedFirstValueBool} `;
        else dataToSaveAsString += `${line.selectedFirstValueInput} `;

        if (line.selectedFirstLogic != "notSelected") {
          dataToSaveAsString += `${line.selectedFirstLogic} `;

          if (line.selectedSecondValueBool != "notSelected")
            dataToSaveAsString += `1'b${line.selectedSecondValueBool} `;
          else dataToSaveAsString += `${line.selectedSecondValueInput} `;

          if (line.selectedSecondLogic != "notSelected") {
            if (
              line.selectedSign.indexOf("(") == -1 &&
              line.selectedFirstLogic.indexOf("(") == -1
            ) {
              line.selectedSecondLogic = line.selectedSecondLogic.replace(
                `)`,
                ""
              );
              dataToSaveAsString += `${line.selectedSecondLogic} `;
            } else {
              dataToSaveAsString += `${line.selectedSecondLogic} `;
            }
            if (line.selectedThirdValueBool != "notSelected")
              dataToSaveAsString += `1'b${line.selectedThirdValueBool} `;
            else dataToSaveAsString += `${line.selectedThirdValueInput} `;
          }
        }
        if (
          line.selectedSecondValueBool != "notSelected" ||
          line.selectedSecondValueInput != undefined
        )
          dataToSaveAsString += `)${line.closingBrackets}`;
        else dataToSaveAsString += `${line.closingBrackets}`;
        if (line.selectedFinalLogic != "notSelected")
          dataToSaveAsString += `${line.selectedFinalLogic}`;
        if (line.length - 1 != index) dataToSaveAsString += `\n`;
      });
      return true;
    } else {
      alert(result);
      return false;
    }
  };
};

// The Output table and line is simmilar in implementation to the Transition table while having several key differences
// All differences are described below
// - The output table does not have any final logic as each line is separate
// - The output table does not contain any validation as there is no need of it
// - Enabling submiting and adding more lines is handled without the need for checking the final logic as it is not in the table
var OutputLine = function () {
  var self = this;
  self.inputs = ko.observable();
  self.selectedFirstValueBool = ko.observable();
  self.selectedFirstValueInput = ko.observable();
  self.selectedFirstLogic = ko.observable();
  self.selectedSecondValueBool = ko.observable();
  self.selectedSecondValueInput = ko.observable();
  self.selectedSecondLogic = ko.observable();
  self.selectedThirdValueBool = ko.observable();
  self.selectedThirdValueInput = ko.observable();
  self.selectedFinalLogic = ko.observable();
  self.closingBrackets = ko.computed(function () {
    var bracketNumber = 0;
    if (
      self.selectedFirstLogic() == "&&(" ||
      self.selectedFirstLogic() == "||("
    )
      bracketNumber++;

    if (
      self.selectedSecondLogic() == ")&&" ||
      self.selectedSecondLogic() == ")||"
    )
      bracketNumber--;

    switch (bracketNumber) {
      case 0:
        return "";
      case 1:
        return ")";
      default:
        return "";
    }
  });
  self.enableFinalLogic = ko.computed(function () {
    if (
      (self.selectedFirstValueBool() != "notSelected" ||
        self.selectedFirstValueInput() != undefined) &&
      self.selectedFirstLogic() == "notSelected"
    )
      return true;
    else if (
      (self.selectedSecondValueBool() != "notSelected" ||
        self.selectedSecondValueInput() != undefined) &&
      self.selectedSecondLogic() == "notSelected"
    )
      return true;
    else if (
      self.selectedThirdValueBool() != "notSelected" ||
      self.selectedThirdValueInput() != undefined
    )
      return true;
    else {
      return false;
    }
  });
  self.inputs.subscribe(function () {
    if (self.inputs() == undefined) {
      self.selectedFirstValueBool("notSelected");
      self.selectedFirstValueInput(undefined);
    }
  });
  self.selectedFirstValueBool.subscribe(function () {
    if (self.selectedFirstValueBool() == "notSelected") {
      self.selectedFirstLogic("notSelected");
    }
  });
  self.selectedFirstValueInput.subscribe(function () {
    if (self.selectedFirstValueInput() == undefined) {
      self.selectedFirstLogic("notSelected");
    }
  });
  self.selectedFirstLogic.subscribe(function () {
    if (self.selectedFirstLogic() == "notSelected") {
      self.selectedSecondValueBool("notSelected");
      self.selectedSecondValueInput(undefined);
    }
  });
  self.selectedSecondValueBool.subscribe(function () {
    if (self.selectedSecondValueBool() == "notSelected") {
      self.selectedSecondLogic("notSelected");
    }
  });
  self.selectedSecondValueInput.subscribe(function () {
    if (self.selectedSecondValueInput() == undefined) {
      self.selectedSecondLogic("notSelected");
    }
  });
  self.selectedSecondLogic.subscribe(function () {
    if (self.selectedSecondLogic() == "notSelected") {
      self.selectedThirdValueBool("notSelected");
      self.selectedThirdValueInput(undefined);
    }
  });
};

var OutputTable = function () {
  var self = this;
  self.lines = ko.observableArray();
  self.availableInputs = ko.pureComputed(function () {
    return vm.availableInputs();
  });
  self.availableOutputs = ko.pureComputed(function () {
    return vm.availableOutputs();
  });

  self.currentFrom = ko.pureComputed(function () {
    return vm.currentFrom();
  });

  // Operations
  self.addLine = function () {
    self.lines.push(new OutputLine());
  };
  self.removeLine = function (line) {
    self.lines.remove(line);
  };

  self.removeLines = function () {
    self.lines.removeAll();
  };

  self.verifySubmit = ko.computed(function () {
    var found = false;
    self.lines().forEach((line) => {
      if (line.enableFinalLogic() == false) {
        found = true;
      }
    });
    if (found == false && self.lines().length != 0) return true;
    else return false;
  });

  self.verifyAdd = ko.computed(function () {
    console.log(self.lines());
    if (self.lines().length != 0)
      if (
        self.lines()[self.lines().length - 1].selectedFinalLogic() ==
        "notSelected"
      ) {
        return false;
      } else {
        return true;
      }
    else return true;
  });

  self.save = function () {
    dataToSaveAsString = "";
    var dataToSave = $.map(self.lines(), function (line) {
      return {
        inputs: line.inputs(),
        selectedFirstValueBool: line.selectedFirstValueBool(),
        selectedFirstValueInput: line.selectedFirstValueInput(),
        selectedFirstLogic: line.selectedFirstLogic(),
        selectedSecondValueBool: line.selectedSecondValueBool(),
        selectedSecondValueInput: line.selectedSecondValueInput(),
        selectedSecondLogic: line.selectedSecondLogic(),
        selectedThirdValueBool: line.selectedThirdValueBool(),
        selectedThirdValueInput: line.selectedThirdValueInput(),
        closingBrackets: line.closingBrackets(),
      };
    });

    dataToSave.forEach(function (line, index) {
      if (
        line.selectedSecondValueBool != "notSelected" ||
        line.selectedSecondValueInput != undefined
      )
        dataToSaveAsString += `${line.inputs} = (`;
      else dataToSaveAsString += `${line.inputs} = `;

      if (line.selectedFirstValueBool != "notSelected")
        dataToSaveAsString += `1'b${line.selectedFirstValueBool} `;
      else dataToSaveAsString += `${line.selectedFirstValueInput} `;

      if (line.selectedFirstLogic != "notSelected") {
        dataToSaveAsString += `${line.selectedFirstLogic} `;

        if (line.selectedSecondValueBool != "notSelected")
          dataToSaveAsString += `1'b${line.selectedSecondValueBool} `;
        else dataToSaveAsString += `${line.selectedSecondValueInput} `;

        if (line.selectedSecondLogic != "notSelected") {
          if (
            line.selectedSign.indexOf("(") == -1 &&
            line.selectedFirstLogic.indexOf("(") == -1
          ) {
            line.selectedSecondLogic = line.selectedSecondLogic.replace(
              `)`,
              ""
            );
            dataToSaveAsString += `${line.selectedSecondLogic} `;
          } else {
            dataToSaveAsString += `${line.selectedSecondLogic} `;
          }
          if (line.selectedThirdValueBool != "notSelected")
            dataToSaveAsString += `1'b${line.selectedThirdValueBool} `;
          else dataToSaveAsString += `${line.selectedThirdValueInput} `;
        }
      }
      if (
        line.selectedSecondValueBool != "notSelected" ||
        line.selectedSecondValueInput != undefined
      )
        dataToSaveAsString += `)${line.closingBrackets} \n`;
      else dataToSaveAsString += `${line.closingBrackets} \n`;
    });
  };
};

// output end

var lSVGInAString;
// This is the format of the FSM that will be used by State Machine Cat to create the FSM picture
// When the page is started up this serves as the example view
var jsonFSM = {
  transitions: [],
  states: [
    {
      name: "state2",
      type: "regular",
    },
    {
      name: "state1",
      type: "regular",
    },
  ],
};
try {
  lSVGInAString = render(jsonFSM, {
    outputType: "svg",
    inputType: "json",
  });
} catch (pError) {
  console.error(pError);
}
// The vm represents the viewmodel used by the KnockoutJS framework
var vm = {
  logo: ko.observable(lSVGInAString),
  availableStates: ko.observableArray(["state1", "state2"]),
  inputsEnabled: ko.observable(true),
  checkedReset: ko.observable(false),
  availableInputs: ko.observableArray([]),
  availableOutputs: ko.observableArray([]),
  currentFrom: ko.observable(),
  currentTo: ko.observable(),
  verilogCode: ko.observable(),
};
// These represent the tables view models
var currentTransitionTable = new TransitionTable();
var outputsTable = new OutputTable();
// The master VM encapsulates both VMs as a HTML page cannot support 2 viewmodels.
// The masterVM is applied and the child VMs can then be accessed
var masterVM = {
  states: vm,
  table: currentTransitionTable,
  outputs: outputsTable,
};

ko.applyBindings(masterVM);
let currentDeleteAction = "";
let currentEnvironmentAction = "";

// Below are the different handlers for buttons
// Most of them open modals for adding deleting editing states transitions etc
$(".download-btn").click(() => {
  downloadToFile();
});

$(".show-tutorial-btn").click(() => {
  $("#instructions-form-modal .modal-title").text("Instructions");
  $("#instructions-form")[0].reset();
  $("#instructions-form-modal").modal("show");
});

$(".add-state-btn").click(() => {
  $("#state-form-modal .modal-title").text("Add State");
  $("#state-form")[0].reset();
  $("#state-form-modal").modal("show");
  currentEnvironmentAction = "Add";
});

$(".edit-state-btn").click(() => {
  $("#editState-form-modal .modal-title").text("Edit State");
  $("#editState-form")[0].reset();
  $("#editState-form-modal").modal("show");
});

$(".add-inputs-btn").click(() => {
  $("#addInputs-form-modal .modal-title").text("Add Inputs");
  $("#addInputs-form")[0].reset();
  $("#addInputs-form-modal").modal("show");
  currentEnvironmentAction = "Add";
});

$(".delete-state-btn").click(() => {
  $("#deleteState-form-modal .modal-title").text("Delete State");
  $("#deleteState-form")[0].reset();
  $("#deleteState-form-modal").modal("show");
  currentDeleteAction = "state";
});

$(".delete-transition-btn").click(() => {
  $("#deleteTransition-form-modal .modal-title").text("Delete Transition");
  $("#deleteTransition-form")[0].reset();
  $("#deleteTransition-form-modal").modal("show");
});
// Delete state and assigned output button share the same modal that is modified on startup
$(".delete-assignedOutput-btn").click(() => {
  $("#deleteState-form-modal .modal-title").text("Delete Output");
  $("#deleteState-form .col-form-label").text(
    "State whose outputs will be deleted"
  );
  $("#deleteState-form")[0].reset();
  $("#deleteState-form-modal").modal("show");
  currentDeleteAction = "output";
});

$(".add-transition-btn").click(() => {
  $("#addTransition-form-modal .modal-title").text("Add Transition");
  dataToSaveAsString = "";
  masterVM.states.currentFrom(masterVM.states.availableStates()[0]);
  masterVM.states.currentTo(masterVM.states.availableStates()[0]);
  $("#addTransition-form")[0].reset();
  $("#addTransition-form-modal").modal("show");
  masterVM.table.removeLines();
  masterVM.table.addLine();
});

$(".add-outputsToState-btn").click(() => {
  $("#addOutputs-form-modal .modal-title").text("Add Outputs");
  dataToSaveAsString = "";
  $("#addOutputs-form")[0].reset();
  $("#addOutputs-form-modal").modal("show");
  masterVM.outputs.removeLines();
  masterVM.outputs.addLine();
});

$(".add-outputs-btn").click(() => {
  $("#addOutputsToFSM-form-modal .modal-title").text("Add Outputs to FSM");
  dataToSaveAsString = "";
  $("#addOutputsToFSM-form")[0].reset();
  $("#addOutputsToFSM-form-modal").modal("show");
});

// Below are the functions that handle the submitting of modals

// The add state modal will filter any already existing states and illegal characters from the ones submitted
$("#state-form").on("submit", (e) => {
  e.preventDefault();
  const stateForm = {};
  $.each($("#state-form").serializeArray(), (i, field) => {
    stateForm[field.name] = field.value;
  });
  // Remove all illegal characters and split by commas
  const statesToAdd = stateForm["state-name"]
    .replace(/[`~!@#$%^&*()_|+\-=?;:'".<>\{\}\[\]\\\/]/gi, "")
    .split(",")
    .filter(
      (x) =>
        x !== "" &&
        vm.availableStates().find((state) => state == x) == undefined
    );
  statesToAdd.forEach((state) => {
    if (
      jsonFSM.states.find(
        (jsonState) => jsonState.name == state.toLowerCase()
      ) == undefined
    ) {
      jsonFSM.states.push({ name: state.toLowerCase(), type: "regular" });
      vm.availableStates.push(state.toLowerCase());
    }
  });

  refreshFSM();
  $("#state-form-modal").modal("hide");
});
// The add inputs modal will filter any already existing inputs and illegal characters from the ones submitted
$("#addInputs-form").on("submit", (e) => {
  e.preventDefault();
  const addInputsForm = {};
  $.each($("#addInputs-form").serializeArray(), (i, field) => {
    addInputsForm[field.name] = field.value;
  });
  // Remove all whitespace and split by commas
  const inputsToAdd = addInputsForm["input-name"]
    .replace(/[`~!@#$%^&*()_|+\-=?;:'".<>\{\}\[\]\\\/]/gi, "")
    .split(",")
    .filter(
      (x) =>
        x !== "" &&
        vm.availableInputs().find((input) => input == x) == undefined
    );
  inputsToAdd.forEach((input) => {
    if (
      vm
        .availableInputs()
        .find((availableInput) => availableInput == input.toLowerCase()) ==
      undefined
    )
      vm.availableInputs.push(input.toLowerCase());
  });

  $("#addInputs-form-modal").modal("hide");
});
// The add outputs modal will filter any already existing outputs and illegal characters from the ones submitted
$("#addOutputsToFSM-form").on("submit", (e) => {
  e.preventDefault();
  const addOutputsForm = {};
  $.each($("#addOutputsToFSM-form").serializeArray(), (i, field) => {
    addOutputsForm[field.name] = field.value;
  });
  const outputsToAdd = addOutputsForm["addOutputsToFSM-name"]
    .replace(/[`~!@#$%^&*()_|+\-=?;:'".<>\{\}\[\]\\\/]/gi, "")
    .split(",")
    .filter(
      (x) =>
        x !== "" &&
        vm.availableInputs().find((output) => output == x) == undefined
    );
  outputsToAdd.forEach((output) => {
    if (
      vm
        .availableOutputs()
        .find((availableOutput) => availableOutput == output.toLowerCase()) ==
      undefined
    )
      vm.availableOutputs.push(output.toLowerCase());
  });

  $("#addOutputsToFSM-form-modal").modal("hide");
});

// Editing states requires modifying all transitions paramaters as in the state from and to if any of them
// are the state being edited
// Also the data has to be updated in multiple arrays relating to the viewmodels
$("#editState-form").on("submit", (e) => {
  e.preventDefault();
  const editStateForm = {};
  $.each($("#editState-form").serializeArray(), (i, field) => {
    editStateForm[field.name] = field.value;
  });

  const stateToEdit = $("#editState-name").find(":selected").text();
  const stateToAdd = editStateForm["editState-change"].replace(
    /[|&;$%@"<>()+,]/g,
    ""
  );
  jsonFSM.states.find((state) => state.name === stateToEdit).name = stateToAdd;
  jsonFSM.transitions.forEach((transition) => {
    if (transition.from == stateToEdit && transition.to !== stateToEdit)
      transition.from = stateToAdd;
    else if (transition.from !== stateToEdit && transition.to == stateToEdit)
      transition.to = stateToAdd;
    else if (transition.from == stateToEdit && transition.to == stateToEdit) {
      transition.from = stateToAdd;
      transition.to = stateToAdd;
    }
  });

  if (transitionsTable != []) {
    transitionsTable.forEach((transition) => {
      if (transition.from == stateToEdit && transition.to !== stateToEdit)
        transition.from = stateToAdd;
      else if (transition.from !== stateToEdit && transition.to == stateToEdit)
        transition.to = stateToAdd;
      else if (transition.from == stateToEdit && transition.to == stateToEdit) {
        transition.from = stateToAdd;
        transition.to = stateToAdd;
      }
    });
  }

  if (outputsForStates != []) {
    outputsForStates.forEach((output) => {
      if (output.state == stateToEdit) output.state == stateToAdd;
    });
  }
  vm.availableStates.replace(stateToEdit, stateToAdd);
  refreshFSM();

  $("#editState-form-modal").modal("hide");
});
// Delete state modal has to remove all transition containing that state along with the state itself
$("#deleteState-form").on("submit", (e) => {
  e.preventDefault();

  const stateToDelete = $("#deleteState-name").find(":selected").text();

  jsonFSM.states.splice(
    jsonFSM.states.findIndex((state) => state.name === stateToDelete),
    1
  );
  var tempTransitions = [];
  if (currentDeleteAction == "state") {
    jsonFSM.transitions.forEach((transition) => {
      if (transition.from !== stateToDelete && transition.to !== stateToDelete)
        tempTransitions.push(transition);
    });
    jsonFSM.transitions = tempTransitions;
    tempTransitions = [];
    if (transitionsTable != []) {
      transitionsTable.forEach((transition) => {
        if (
          transition.from !== stateToDelete &&
          transition.to !== stateToDelete
        )
          tempTransitions.push(transition);
      });
      transitionsTable = tempTransitions;
      tempTransitions = [];
    }
    vm.availableStates.splice(
      vm.availableStates().findIndex((state) => state == stateToDelete),
      1
    );
  }
  if (outputsForStates != []) {
    outputsForStates.forEach((output) => {
      if (output.state !== stateToDelete) tempTransitions.push(output);
    });
    outputsForStates = tempTransitions;
  }

  refreshFSM();
  $("#deleteState-form-modal").modal("hide");
});

$("#deleteTransition-form").on("submit", (e) => {
  e.preventDefault();
  // Remove all whitespace and split by commas
  const stateFrom = $("#deleteTransition-from").find(":selected").text();
  const stateTo = $("#deleteTransition-to").find(":selected").text();
  var tempTransitions = [];
  jsonFSM.transitions.forEach((transition) => {
    if (transition.from !== stateFrom || transition.to !== stateTo)
      tempTransitions.push(transition);
  });
  jsonFSM.transitions = tempTransitions;
  tempTransitions = [];
  if (transitionsTable != []) {
    transitionsTable.forEach((transition) => {
      if (transition.from !== stateFrom || transition.to !== stateTo)
        tempTransitions.push(transition);
    });
    transitionsTable = tempTransitions;
  }
  refreshFSM();
  $("#deleteTransition-form-modal").modal("hide");
});
// When the transition is submitted the save function is called which validates and creates the string
// If everything is ok with the transition then it gets added into the jsonFSM
$("#addTransition-form").on("submit", (e) => {
  e.preventDefault();
  if (masterVM.table.save() == true) {
    const stateFrom = $("#addTransition-from").find(":selected").text();
    const stateTo = $("#addTransition-to").find(":selected").text();
    vm.inputsEnabled(false);

    const transitionToBeRemoved = jsonFSM.transitions.find(
      (transition) => transition.from === stateFrom && transition.to === stateTo
    );
    const transitionIndex = jsonFSM.transitions.indexOf(transitionToBeRemoved);
    if (transitionIndex != -1) {
      jsonFSM.transitions.splice(transitionIndex, 1);
      jsonFSM.transitions.push({
        from: stateFrom,
        to: stateTo,
        label: dataToSaveAsString,
        event: dataToSaveAsString,
      });
    } else {
      jsonFSM.transitions.push({
        from: stateFrom,
        to: stateTo,
        label: dataToSaveAsString,
        event: dataToSaveAsString,
      });
    }

    refreshFSM();
    $("#addTransition-form-modal").modal("hide");
  }
});

// Same as above
$("#addOutputs-form").on("submit", (e) => {
  e.preventDefault();
  masterVM.outputs.save();
  const stateFor = $("#addOutputs-for").find(":selected").text();

  const ouputsToBeRemoved = outputsForStates.find(
    (outputs) => outputs.state === stateFor
  );
  const outputsIndex = outputsForStates.indexOf(ouputsToBeRemoved);
  if (outputsIndex != -1) {
    outputsForStates.splice(outputsIndex, 1);
    outputsForStates.push({
      state: stateFor,
      outputs: dataToSaveAsString,
    });
  } else {
    outputsForStates.push({
      state: stateFor,
      outputs: dataToSaveAsString,
    });
  }
  $("#addOutputs-form-modal").modal("hide");
});

$("#selectFiles").change(function () {
  var file = $("#selectFiles")[0].files[0].name;
  $("#selectFilesLabel").text(file);
});

// When called the state machine cat library will create the svg picture of the FSM
// As a parameter the direction is left to right to aid readability
function refreshFSM() {
  try {
    lSVGInAString = render(jsonFSM, {
      inputType: "json",
      outputType: "svg",
      direction: "left-right",
    });
    vm.logo(lSVGInAString);
  } catch (pError) {
    console.error(pError);
  }
}

$(".calculate-picture-btn").click(() => {
  vm.verilogCode(compileVerilog(jsonFSM, vm, outputsForStates));
});
// Downloads the FSM and aditional data in a json File
const downloadToFile = () => {
  downloadableFSM.jsonFSM = jsonFSM;
  downloadableFSM.outputsForStates = outputsForStates;
  downloadableFSM.availableInputs = vm.availableInputs();
  downloadableFSM.availableStates = vm.availableStates();
  downloadableFSM.availableOutputs = vm.availableOutputs();
  downloadableFSM.transitionsTable = transitionsTable;
  const a = document.createElement("a");
  const file = new Blob([JSON.stringify(downloadableFSM)], {
    type: "application/json",
  });
  a.href = URL.createObjectURL(file);
  a.download = "yourFSM.json";
  a.click();

  URL.revokeObjectURL(a.href);
};

// Imports json Files and gets the page to the state described in the file
document.getElementById("import").onclick = function () {
  var files = document.getElementById("selectFiles").files;
  if (files.length <= 0) {
    return false;
  }

  var fr = new FileReader();

  fr.onload = function (e) {
    var result = JSON.parse(e.target.result);
    vm.availableStates(result.availableStates);
    vm.availableInputs(result.availableInputs);
    vm.availableOutputs(result.availableOutputs);
    transitionsTable = result.transitionsTable;
    outputsForStates = result.outputsForStates;
    jsonFSM = result.jsonFSM;
    refreshFSM();
  };

  fr.readAsText(files.item(0));
};
